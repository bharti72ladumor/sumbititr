import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class ServercallService {

  constructor(private http:HttpClient) { }

  baseUrl='https://pdskp.elb.cisinlive.com';

 
  fnSendGetRequest(url){
    return this.http.get(this.baseUrl+url);
    
  }
    
  fnSendPostRequest(url,data){
   
    return this.http.post(this.baseUrl+url,data);
        
}

}
