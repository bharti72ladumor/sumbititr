import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { ServercallService } from './servercall.service'

@Injectable({
  providedIn: 'root'
})
export class SharedService {
 private toasterMsgSub = new Subject();

 constructor(private router: Router, private servicecall: ServercallService) { }
 
  fnSetToasterMsg(msg, clr) {
    let dataObj = {
      'msg': msg,
      'bg': clr
    }
    this.toasterMsgSub.next(dataObj);
  }
  fnToasterObservable() {
    return this.toasterMsgSub.asObservable();
  }  
}
