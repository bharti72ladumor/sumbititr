import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { DynamicAddInputFields } from '../../grid.model';

@Component({
  selector: 'personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.css']
})
export class PersonalDetailsComponent implements OnInit {

  constructor( private toastr:ToastrService) { }

  dynamicBankArray:Array<DynamicAddInputFields> = [];
  newBankDynamic: any = {};

  ngOnInit():void {
   
    this.newBankDynamic = {bankname:"",accountnumber:"",ifsccode:""};
    this.dynamicBankArray.push(this.newBankDynamic);
}

addBankInput(index){
  this.newBankDynamic = {bankname:"",accountnumber:"",ifsccode:""};
  this.dynamicBankArray.push(this.newBankDynamic);
  console.log(this.dynamicBankArray);
}

deleteBankInput(index){
  if(this.dynamicBankArray.length == 1){
    this.toastr.error("can't delete this" );
    return false;
  }else{
    this.dynamicBankArray.splice(index, 1);
    return true;
  }
}



}
