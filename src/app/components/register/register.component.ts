import { Component, OnInit,Directive, forwardRef, Attribute,OnChanges, SimpleChanges,Input } from '@angular/core';
import { Router } from '@angular/router';


import { NgForm } from '@angular/forms';
import { ServercallService } from '../../Services/servercall.service';
import { SharedService } from '../../Services/shared.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  isSendOTP = false;
  otp: string;
  dataobj = {};
  userObjResponse = {};

  constructor(
      private router: Router ,
      private servicecall:ServercallService,
      private sharedservice:SharedService,
      private toastr:ToastrService) {

  }

  ngOnInit() {
  }

  sendotp(registerForm) {
    this.isSendOTP = false;
    this.otp = undefined;
    this.userObjResponse = undefined;
    this.servicecall.fnSendPostRequest('/user/save?isOTPVerified=true',this.dataobj)
      .subscribe((res:any)=>{
        
        if (res.statusCode == 'ITR200') {
          this.toastr.success('Send OTP Successfully');
          this.isSendOTP = true;
          this.userObjResponse = res.results;
          this.resetForm(registerForm);
        } else {
          this.toastr.error(res.statusMessage);
        }
      },
        error=>{
          this.dataobj = {}
          this.toastr.error('Something went wrong','red');
      });
  }

  verifyOTP(){
    this.servicecall.fnSendGetRequest('/api/otp/verify?otp='+ this.otp + "&userId=" + this.userObjResponse['id'])
      .subscribe((res:any)=>{
        
        if (res.statusCode == 'ITR200') {
          this.toastr.success(res.results);
          this.router.navigate(['/login']);
        } else if (res.statusCode == 'ITR116') {
          this.toastr.error(res.statusMessage);
        } else {
          this.toastr.error(res.results);
        }
      },
        error=>{
          this.toastr.error('Something went wrong', error);
      });
  }

  resetForm(registerForm:NgForm){
    this.dataobj = {};
    registerForm.reset();
 }
}
        


    







