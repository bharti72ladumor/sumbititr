import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateContactdetailsComponent } from './update-contactdetails.component';

describe('UpdateContactdetailsComponent', () => {
  let component: UpdateContactdetailsComponent;
  let fixture: ComponentFixture<UpdateContactdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateContactdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateContactdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
