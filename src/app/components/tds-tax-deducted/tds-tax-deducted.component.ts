import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { DynamicGridSecond } from '../../grid.model';

@Component({
  selector: 'tds-deducted',
  templateUrl: './tds-tax-deducted.component.html',
  styleUrls: ['./tds-tax-deducted.component.css']
})
export class TdsTaxDeductedComponent implements OnInit {

  constructor(private toastr:ToastrService) { }
  
   dynamicArraysecond:Array<DynamicGridSecond> = [];
   newDynamicsecond: any = {};
  ngOnInit():void {
    this.newDynamicsecond = {row1:"",row2:"",row3:"", row4:"",row5:"",row6:""};
    this.newDynamicsecond.push(this.newDynamicsecond);
  }
  addRowsecond(index){
    this.newDynamicsecond = {row1:"",row2:"",row3:"", row4:"",row5:"",row6:""};
    this.dynamicArraysecond.push(this.newDynamicsecond);  
    console.log(this.dynamicArraysecond);
    return true;
  }
    deleteRowsecond(index){
      if(this.dynamicArraysecond.length == 1){
        this.toastr.error("cant delete this row");
        return false;
      }else{
        this.dynamicArraysecond.splice(index, 1);
        return true;
      }
      }
    }

