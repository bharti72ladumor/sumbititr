import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

// show/hide boolean variable
showPassword: boolean;

constructor() {
  // init value in constructor
  this.showPassword = false;
}

// click event, show/hide based on checkbox value
showHidePassword(e) {
    this.showPassword = e.target.checked;
}

  ngOnInit() {
  }

}
