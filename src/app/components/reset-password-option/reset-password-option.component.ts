import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'reset-password-option',
  templateUrl: './reset-password-option.component.html',
  styleUrls: ['./reset-password-option.component.css']
})
export class ResetPasswordOptionComponent implements OnInit {


  currDiv: string = 'A';
  
  constructor() { }
  
  ShowDiv(divVal: string) {
    this.currDiv = divVal;
  }
 
  ngOnInit() {

  
  }
 
}