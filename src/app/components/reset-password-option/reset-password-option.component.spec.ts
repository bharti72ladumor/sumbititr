import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetPasswordOptionComponent } from './reset-password-option.component';

describe('ResetPasswordOptionComponent', () => {
  let component: ResetPasswordOptionComponent;
  let fixture: ComponentFixture<ResetPasswordOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetPasswordOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetPasswordOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
