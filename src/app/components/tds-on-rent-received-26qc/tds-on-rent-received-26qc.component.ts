import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { DynamicGridfive } from '../../grid.model';

@Component({
  selector: 'tds-rent',
  templateUrl: './tds-on-rent-received-26qc.component.html',
  styleUrls: ['./tds-on-rent-received-26qc.component.css']
})
export class TdsOnRentReceived26qcComponent implements OnInit {

  constructor(private toastr:ToastrService) { }
  
  dynamicArrayFive:Array<DynamicGridfive> = [];
   newDynamicfive:any = {};
  ngOnInit():void {
    this.newDynamicfive = {row1:"",row2:"",row3:"", row4:"",row5:"",row6:""};
    this.newDynamicfive.push(this.newDynamicfive);
  }
  addmixRow(index){
    this.newDynamicfive = {row1:"",row2:"",row3:"", row4:"",row5:"",row6:""};
    this.dynamicArrayFive.push(this.newDynamicfive);  
    console.log(this.dynamicArrayFive);
    return true;
  }
    deletemixRow(index){
      if(this.dynamicArrayFive.length == 1){
        this.toastr.error("cant delete this row");
        return false;
      }else{
        this.dynamicArrayFive.splice(index, 1);
        return true;
      }
      }
}
