import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { DynamicGrid } from '../../grid.model';

@Component({
  selector: 'TDS',
  templateUrl: './tax-deducted-from-salary.component.html',
  styleUrls: ['./tax-deducted-from-salary.component.css']
})
export class TaxDeductedFromSalaryComponent implements OnInit {

  constructor(private toastr: ToastrService) { }  
  
  dynamicArray: Array<DynamicGrid> = [];  
  newDynamic: any = {};  
  ngOnInit(): void {  
      this.newDynamic = {title1: "", title2: "",title3:"",title4:""};  
      this.dynamicArray.push(this.newDynamic);  
  }  
  
  addRow(index) {    
      this.newDynamic = {title1: "", title2: "",title3:"",title4:""};  
      this.dynamicArray.push(this.newDynamic);  
      console.log(this.dynamicArray);  
      return true;  
  }  
    
  deleteRow(index) {  
      if(this.dynamicArray.length ==1) {  
        this.toastr.error("Can't delete the row when there is only one row", 'Warning');  
          return false;  
      } else {  
          this.dynamicArray.splice(index, 1);  
          return true;  
      }  
  }  
  

}
