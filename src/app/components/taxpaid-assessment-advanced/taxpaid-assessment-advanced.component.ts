import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { DynamicGridthird } from '../../grid.model';


@Component({
  selector: 'assessment-advanced',
  templateUrl: './taxpaid-assessment-advanced.component.html',
  styleUrls: ['./taxpaid-assessment-advanced.component.css']
})
export class TaxpaidAssessmentAdvancedComponent implements OnInit {

  constructor(private toastr: ToastrService) {}  
  
  dynamicArraythird: Array<DynamicGridthird> = [];  
  newDynamicthird: any = {};  
  ngOnInit(): void {  
      this.newDynamicthird = {add1:"", add2: "",add3:"",add4:""};  
      this.dynamicArraythird.push(this.newDynamicthird);  
  }  
  
  addMoreRow(index) {    
      this.newDynamicthird = {add1: "", add2: "",add3:"",add4:""};  
      this.dynamicArraythird.push(this.newDynamicthird);  
      console.log(this.dynamicArraythird);  
      return true;  
  }  
    
  deleteMoreRow(index) {  
      if(this.dynamicArraythird.length ==1) {  
        this.toastr.error("Can't delete the row when there is only one row", 'Warning');  
          return false;  
      } else {  
          this.dynamicArraythird.splice(index, 1);  
          return true;  
      }  
  }  
  

}
