import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'deduction80-c',
  templateUrl: './deduction80-c.component.html',
  styleUrls: ['./deduction80-c.component.css']
})
export class Deduction80CComponent implements OnInit {

  isvalid:boolean=true;
  constructor() { }
 
  changevalue(valid){
     this.isvalid=valid;
   }
  
   ngOnInit() {
    
   }
 
}
