import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { DynamicAddInputFields,AddDepositeFields,AddPostFields,AddOtherFields } from '../../grid.model';


@Component({
  selector: 'interest-income',
  templateUrl: './interest-income.component.html',
  styleUrls: ['./interest-income.component.css']
})
export class InterestIncomeComponent implements OnInit {

  constructor( private toastr:ToastrService) { }
   
  dynamicDepositeArray: Array<AddDepositeFields>= [];//fixed deposite fd/interest
  dynamicInterestArray:Array<DynamicAddInputFields> = []; // Bank Interest 
  dynamicPostArray:Array<AddPostFields>= []; //Post Oﬃce Interest 
  dynamicOtherArray:Array<AddOtherFields>=[]; //Other Interest 
  newInterestdynamic: any = {};


  ngOnInit():void {
     
    this.newInterestdynamic = {bankname1:"",amount:""};//bank interest
    this.dynamicInterestArray.push(this.newInterestdynamic); //bank interest
   
   // deposite
    this.newInterestdynamic = {bankname2:"",amount1:""};
    this.dynamicDepositeArray.push(this.newInterestdynamic);
    //end

    //post office interest 
    this.newInterestdynamic = {details1:"",amount2:""};
    this.dynamicPostArray.push(this.newInterestdynamic);
   //end

   //other interest 
   this.newInterestdynamic= {details2:"",amount3:""};
   this.dynamicOtherArray.push(this.newInterestdynamic);
   //end

  }
// dynamic add/reomve fields of Bank Interest
addInterest(index){
 this.newInterestdynamic = {bankname1:"",amount:""};
 this.dynamicInterestArray.push(this.newInterestdynamic);
console.log(this.dynamicInterestArray);
  }


deleteInterest(index){
  if(this.dynamicInterestArray.length == 1){
    this.toastr.error("cant delete this field");
    return false;
  }else{
    this.dynamicInterestArray.splice(index, 1);
    return true;
  }
}
//end of bank interests function

//start of the interest deposite
addDeposite(index){
  this.newInterestdynamic = {bankname2:"",amount1:""};
  this.dynamicDepositeArray.push(this.newInterestdynamic);
 console.log(this.dynamicDepositeArray);
   }
   
   
   deleteDeposite(index){
    if(this.dynamicDepositeArray.length == 1){
      this.toastr.error("cant delete this field");
      return false;
    }else{
      this.dynamicDepositeArray.splice(index, 1);
      return true;
    }
  }
//end of the interest deposite


// add remove fields in other interest
addOther(index){
  this.newInterestdynamic = {details2:"",amount3:""};
  this.dynamicOtherArray.push(this.newInterestdynamic);
  console.log(this.dynamicOtherArray);
}

deleteOther(index){
  if(this.dynamicOtherArray.length == 1){
    this.toastr.error("cant delete this ");
    return false;
  }else{
    this.dynamicOtherArray.splice(index,1);
    return true;
  }
}
//end
//add remove fields in post office interest  
addPost(index){
  this.newInterestdynamic = {details1:"",amount2:""};
  this.dynamicPostArray.push(this.newInterestdynamic);
 console.log(this.dynamicPostArray);
   }
   
   
   deletePost(index){
    if(this.dynamicPostArray.length == 1){
      this.toastr.error("cant delete this field");
      return false;
    }else{
      this.dynamicPostArray.splice(index, 1);
      return true;
    }
 //end of post office interest
  }












// end of fields in post office interest

}
