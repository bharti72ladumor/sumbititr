import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';  
import { DynamicGridFourth } from '../../grid.model';


@Component({
  selector: 'TCS',
  templateUrl: './tcs-tax-collected.component.html',
  styleUrls: ['./tcs-tax-collected.component.css']
})
export class TcsTaxCollectedComponent implements OnInit {

  constructor(private toastr: ToastrService) {}  
  
  dynamicArrayfourth: Array<DynamicGridFourth> = [];  
  newDynamicfourth: any = {};  
  ngOnInit(): void {  
      this.newDynamicfourth = {sub1:"", sub2: "",sub3:"",sub4:"",sub5:""};  
      this.dynamicArrayfourth.push(this.newDynamicfourth);  
  }  
  
  addsubRow(index) {    
      this.newDynamicfourth =  {sub1:"", sub2: "",sub3:"",sub4:"",sub5:""};    
      this.dynamicArrayfourth.push(this.newDynamicfourth);  
      console.log(this.dynamicArrayfourth);  
      return true;  
  }  
    
  deletesubRow(index) {  
      if(this.dynamicArrayfourth.length ==1) {  
        this.toastr.error("Can't delete the row when there is only one row", 'Warning');  
          return false;  
      } else {  
          this.dynamicArrayfourth.splice(index, 1);  
          return true;  
      }  
  }  

}
