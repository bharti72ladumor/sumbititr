import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";

@Component({
  selector: 'form16',
  templateUrl: './form16.component.html',
  styleUrls: ['./form16.component.css']
})
export class Form16Component implements OnInit {

  constructor() { }

  ngOnInit():void {
      $("a[href='#bottom']").click(function() {
      $("html, body").animate({ scrollTop: $(document).height() }, "slow");
      return false;
    }); 

    
  }

}
