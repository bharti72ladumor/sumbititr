import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';  
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AccountActivationComponent } from './components/account-activation/account-activation.component';
import { SetPasswordComponent } from './components/set-password/set-password.component';
import { ResetPasswordOptionComponent } from './components/reset-password-option/reset-password-option.component';
import { UpdateContactdetailsComponent } from './components/update-contactdetails/update-contactdetails.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { UploadDocumentsComponent } from './components/upload-documents/upload-documents.component';
import { DownloadDocumentsComponent } from './components/download-documents/download-documents.component';
import { Form16Component } from './components/form16/form16.component';
import { Deduction80CComponent } from './components/deduction80-c/deduction80-c.component';
import { Deduction80DComponent } from './components/deduction80-d/deduction80-d.component';
import { MoreDeductionComponent } from './components/more-deduction/more-deduction.component';
import { TdsOnRentReceived26qcComponent } from './components/tds-on-rent-received-26qc/tds-on-rent-received-26qc.component';
import { TaxDeductedFromSalaryComponent } from './components/tax-deducted-from-salary/tax-deducted-from-salary.component';
import { TdsTaxDeductedComponent } from './components/tds-tax-deducted/tds-tax-deducted.component';
import { TaxpaidAssessmentAdvancedComponent } from './components/taxpaid-assessment-advanced/taxpaid-assessment-advanced.component';
import { TcsTaxCollectedComponent } from './components/tcs-tax-collected/tcs-tax-collected.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HouseDetailsComponent } from './components/house-details/house-details.component';
import { InterestDetailComponent } from './components/interest-detail/interest-detail.component';
import { RentReceivedArearsComponent } from './components/rent-received-arears/rent-received-arears.component';
import { HouseTenantComponent } from './components/house-tenant/house-tenant.component';
import { PersonalDetailsComponent } from './components/personal-details/personal-details.component';
import { Donation80GComponent } from './components/donation80-g/donation80-g.component';
import { IncomeFromSalaryComponent } from './components/income-from-salary/income-from-salary.component';
import { InterestIncomeComponent } from './components/interest-income/interest-income.component';
import { RentalOtherIncomeComponent } from './components/rental-other-income/rental-other-income.component';
import { AgricultureExemptIncomeComponent } from './components/agriculture-exempt-income/agriculture-exempt-income.component';
import { SubmissionComponent } from './components/submission/submission.component';
import { OptionPageComponent } from './components/option-page/option-page.component';

import {HttpClientModule,HTTP_INTERCEPTORS} from '@angular/common/http';

import { ServercallService } from './Services/servercall.service';
import { OptionPage2Component } from './components/option-page2/option-page2.component';








@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    SidebarComponent,
    AccountActivationComponent,
    SetPasswordComponent,
    ResetPasswordOptionComponent,
    UpdateContactdetailsComponent,
    ChangePasswordComponent,
    ContactUsComponent,
    UploadDocumentsComponent,
    DownloadDocumentsComponent,
    Form16Component,
    Deduction80CComponent,
    Deduction80DComponent,
    MoreDeductionComponent,
    TdsOnRentReceived26qcComponent,
    TaxDeductedFromSalaryComponent,
    TdsTaxDeductedComponent,
    TaxpaidAssessmentAdvancedComponent,
    TcsTaxCollectedComponent,
    HouseDetailsComponent,
    InterestDetailComponent,
    RentReceivedArearsComponent,
    HouseTenantComponent,
    PersonalDetailsComponent,
    Donation80GComponent,
    IncomeFromSalaryComponent,
    InterestIncomeComponent,
    RentalOtherIncomeComponent,
    AgricultureExemptIncomeComponent,
    SubmissionComponent,
    OptionPageComponent,
    OptionPage2Component,
    
   
   
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot()

  ],
  providers: [ ServercallService, ],
  bootstrap: [AppComponent]
})
export class AppModule { }
