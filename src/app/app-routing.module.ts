import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AccountActivationComponent } from './components/account-activation/account-activation.component';
import { SetPasswordComponent } from './components/set-password/set-password.component';
import { ResetPasswordOptionComponent } from './components/reset-password-option/reset-password-option.component';
import { UpdateContactdetailsComponent } from './components/update-contactdetails/update-contactdetails.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { UploadDocumentsComponent } from './components/upload-documents/upload-documents.component';
import { DownloadDocumentsComponent } from './components/download-documents/download-documents.component';
import { Form16Component } from './components/form16/form16.component';
import { Deduction80CComponent } from './components/deduction80-c/deduction80-c.component';
import { Deduction80DComponent } from './components/deduction80-d/deduction80-d.component';
import { MoreDeductionComponent } from './components/more-deduction/more-deduction.component';
import { TdsOnRentReceived26qcComponent } from './components/tds-on-rent-received-26qc/tds-on-rent-received-26qc.component';
import { TaxDeductedFromSalaryComponent } from './components/tax-deducted-from-salary/tax-deducted-from-salary.component';
import { TdsTaxDeductedComponent } from './components/tds-tax-deducted/tds-tax-deducted.component';
import { TaxpaidAssessmentAdvancedComponent } from './components/taxpaid-assessment-advanced/taxpaid-assessment-advanced.component';
import { TcsTaxCollectedComponent } from './components/tcs-tax-collected/tcs-tax-collected.component';
import { HouseDetailsComponent } from './components/house-details/house-details.component';
import { InterestDetailComponent } from './components/interest-detail/interest-detail.component';
import { RentReceivedArearsComponent } from './components/rent-received-arears/rent-received-arears.component';
import { HouseTenantComponent } from './components/house-tenant/house-tenant.component';
import { PersonalDetailsComponent } from './components/personal-details/personal-details.component';
import { Donation80GComponent } from './components/donation80-g/donation80-g.component';
import { IncomeFromSalaryComponent } from './components/income-from-salary/income-from-salary.component';
import { InterestIncomeComponent } from './components/interest-income/interest-income.component';
import { RentalOtherIncomeComponent } from './components/rental-other-income/rental-other-income.component';
import { AgricultureExemptIncomeComponent } from './components/agriculture-exempt-income/agriculture-exempt-income.component';
import { SubmissionComponent } from './components/submission/submission.component';
import { OptionPageComponent } from './components/option-page/option-page.component';


const routes: Routes = [
  {path:'', redirectTo:'login',pathMatch:'full'},
  {path:'register',component:RegisterComponent},
  {path:'login',component:LoginComponent},
  {path:'header',component:HeaderComponent},
  {path:'footer',component:FooterComponent},
  {path:'sidebar',component:SidebarComponent},
  {path:'account-activation',component:AccountActivationComponent},
  {path:'set-password',component:SetPasswordComponent},
  {path:'reset-password-option',component:ResetPasswordOptionComponent},
  {path:'update-contactdetails',component:UpdateContactdetailsComponent},
  {path:'change-password',component:ChangePasswordComponent},
  {path:'contact-us',component:ContactUsComponent},
  {path:'upload-documents',component:UploadDocumentsComponent},
  {path:'download-document',component:DownloadDocumentsComponent},
  {path:'form16',component:Form16Component},
  {path:'deduction80-c',component:Deduction80CComponent},
  {path:'deduction-80-d',component:Deduction80DComponent},
  {path:'more-deduction',component:MoreDeductionComponent},
  {path:'tds-on-rent-received-26qc',component:TdsOnRentReceived26qcComponent},
  {path:'tax-deduction-from-salary',component:TaxDeductedFromSalaryComponent},
  {path:'tds-tax-deducted',component:TdsTaxDeductedComponent},
  {path:'taxpaid-assessment-advanced',component:TaxpaidAssessmentAdvancedComponent},
  {path:'tcs-tax-collected',component:TcsTaxCollectedComponent},
  {path:'house-details',component:HouseDetailsComponent},
  {path:'interest-detail',component:InterestDetailComponent},
  {path:'rent-received-arears',component:RentReceivedArearsComponent},
  {path:'house-tenant',component:HouseTenantComponent},
  {path:'personal-details',component:PersonalDetailsComponent},
  {path:'donation80-g',component:Donation80GComponent},
  {path:'income-from-salary',component:IncomeFromSalaryComponent},
  {path:'interest-income',component:InterestIncomeComponent},
  {path:'rental-other-income',component:RentalOtherIncomeComponent},
  {path:'agriculture-exempt-income',component:AgricultureExemptIncomeComponent},
  {path:'submission',component:SubmissionComponent},
  {path:'option-page',component:OptionPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
